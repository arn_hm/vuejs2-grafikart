vuejs2-grafikart
=================
This repository follows the tutorials on Vuejs2 edited by Grafikart ([tuto grafikart](https://www.grafikart.fr/formations/vuejs))

All commits are based on a chapter of this course.

| Chapter name     | Commit (begin) |
| :------- | ----: |
| Discovery | ec3310 |
| Instance    | 1adc2c   |
| Computed properties and watchers     | fdfca7 |
| Directives | 18bf81 |
