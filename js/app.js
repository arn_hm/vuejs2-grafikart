Vue.filter('capitalize', function (value) {
    return value.toUpperCase()
});

Vue.filter('reverse', function (value) {
    return value.split('').reverse().join('')
});

let vm = new Vue({
    el: '#app',
    data: {
        message: "Bonjour",
    }
});